set nocompatible
filetype off

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'gmarik/Vundle.vim'

Plugin 'aklt/plantuml-syntax'
Plugin 'php.vim'
Plugin 'tpope/vim-rails'
Plugin 'scrooloose/nerdtree'
Plugin 'xolox/vim-misc'
Plugin 'othree/html5.vim'
Plugin 'JulesWang/css.vim'
Plugin 'janiczek/vim-latte'
Plugin 'joonty/vdebug'
Plugin 'Yggdroot/indentLine'
Plugin 'vim-scripts/taglist.vim'
Plugin 'groenewege/vim-less'
Plugin 'altercation/vim-colors-solarized.git'
Plugin 'powerline/powerline'
Plugin 'shawncplus/phpcomplete.vim'
Plugin 'joonty/vim-taggatron'
Plugin 'davidhalter/jedi-vim'

" <Leader>T
Plugin 'wincent/Command-T'

" pcf fix file
" pcd fix dir
Bundle 'stephpy/vim-php-cs-fixer'

call vundle#end()
filetype plugin indent on


set backspace=indent,eol,start
set ruler
set showcmd
set hls is
set laststatus=2
syntax on
set number
set ignorecase

" Vzhled
set background=dark
colorscheme koehler

" Podpora mysi
if has('mouse')
	set mouse=a
endif

set cul
set tabstop=2
set softtabstop=2
set shiftwidth=2
set expandtab
set hidden

" Klavesy
nnoremap <Leader>j :bn<CR>
nnoremap <Leader>k :bp<CR>

" NERDTree
map <C-n> :NERDTreeToggle<CR>

" Keyboard
:imap <C-Space> <C-X><C-O>

" Taggatron
let g:tagcommands = {
      \   "php" : {
      \     "tagfile" : "php.tags",
      \     "args" : "-R" 
      \   }
      \ }

" PHP
filetype plugin on
autocmd  FileType  php setlocal omnifunc=phpcomplete#CompletePHP
let php_sql_query=1
let php_htmlInStrings=1

" Ruby
autocmd FileType ruby call g:MyRubySettings()
function g:MyRubySettings ()
    set tabstop=2
    set softtabstop=2
    set shiftwidth=2
    set expandtab
endfunction

